import pandas as pd
from PIL import ImageFont


class Qr:

    def __init__(self, url_google):
        self.url: str = url_google
        self.font = ImageFont.truetype('data/RobotoFlex[slnt,wdth,wght,opsz].ttf', size=30)
        self.data_adress = []
        self.data_urls = []
        self.count = 0

    def read_html(self):
        SHEET_ID = self.url.split('/')[-2]
        url = f'https://docs.google.com/spreadsheets/d/{SHEET_ID}/gviz/tq?tqx=out:csv'
        xl = pd.read_csv(url, encoding='utf-8', skiprows=2)
        xl = xl.loc[
            (xl['Статус выкупа'].str.contains('Готов', na=False)) & (xl['QR-код'].astype(str).str.contains('https'))]
        urls = xl['QR-код']
        adress = xl['Адрес пункта выдачи заказов'].astype(str)

        for _, adress in adress.items():
            adress = adress.replace('/', 'др')
            self.data_adress.append(f'{adress}_{self.count})')
            self.count += 1

        for _, link in urls.items():
            link = link.split('/')[-1]
            try:
                link = link.split('.')[0]
            except Exception:
                pass
            link = f'https://i.paste.pics/{link}.png'
            self.data_urls.append(link)

        zip_list = zip(self.data_urls, self.data_adress)
        zip_values = list(zip_list)

        data_img = {}

        for url, adress in zip_values:
            # response = requests.get(url)
            # img = Image.open(BytesIO(response.content))
            # draw_text = ImageDraw.Draw(img)
            # draw_text.text(
            #     (10, 10),
            #     adress.split('_')[0],
            #     font=self.font
            # )
            # img.save(f'{adress}.png')
            data_img[f'{adress}'] = url
            # img.show()
        return data_img


# try:
#     img.save(f'./address/{address}.png')
# except FileNotFoundError:
#     img.save(f'./address/_{address}.png')

# mydir = 'address/'
# list(map(os.unlink, (os.path.join(mydir, f) for f in os.listdir(mydir))))


if __name__ == '__main__':
    dd = Qr('https://docs.google.com/spreadsheets/d/1N_gbeQAo6c-HTy9Mp0xEJgIsm8-MlYYc/edit#gid=1999504296')
    dd.read_html()

# https://docs.google.com/spreadsheets/d/1UHW7blHCMGbe0GICPVVjB8gy8W8H5aXX/edit#gid=1999504296
