import logging

from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart

from keyboards.default.main_keyboard import main_buttons
from loader import dp


@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    logging.info(f'{message.from_user.full_name} - start bot')
    await message.answer(f"Привет, {message.from_user.full_name}!", reply_markup=main_buttons)
