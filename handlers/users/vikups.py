from io import BytesIO

import requests
from PIL import Image, ImageDraw
from aiogram.dispatcher import FSMContext, filters
from aiogram.types import ReplyKeyboardRemove, Message

from keyboards.default.main_keyboard import main_buttons
from loader import dp, FONT
from states.start_vikup import Vikup
from utils.qr import Qr
import validators
import logging


@dp.message_handler(filters.Text(equals='Выкупы'))
async def vikup(message: Message):
    logging.info(f'{message.from_user.full_name} - start vikups')
    await message.answer('Помогу тебе сформировать фотографии с адресами из гугл таблицы.\n'
                         'Скинь мне ссылку на твои выкупы.', reply_markup=ReplyKeyboardRemove())
    await Vikup.url.set()


@dp.message_handler(state=Vikup.url)
async def parse_url(message: Message, state: FSMContext):
    if validators.url(message.text):
        logging.info(f'---- {message.from_user.full_name} - enter url')
        if 'https://docs.google.com/spreadsheets/' in message.text:
            logging.info(f'-------- {message.from_user.full_name} - enter google sheets')
            try:
                await message.answer('Открываю, считываю, формирую...\n'
                                     'щаща всё будет')
                async with state.proxy() as data:
                    data['url'] = message.text
                qr = Qr(message.text)
                data_img: dict = qr.read_html()

                count = 0
                for adress, url in data_img.items():
                    response = requests.get(url)
                    img = Image.open(BytesIO(response.content))
                    draw_text = ImageDraw.Draw(img)
                    draw_text.text(
                        (10, 10),
                        adress.split('_')[0],
                        font=FONT
                    )
                    with BytesIO() as image_binary:
                        img.save(image_binary, 'PNG')
                        image_binary.seek(0)
                        await message.answer_photo(image_binary, caption=adress.split('_')[0])
                    # await message.answer_document(url, caption=adress.split('_')[0])
                    count += 1

                await message.answer('Собрал все, что было. Удачи!', reply_markup=main_buttons)

                logging.info(f'-------- {message.from_user.full_name} - give for user his qr {count=}')

                await state.finish()
            except TypeError as err:
                logging.info(f'-------- {message.from_user.full_name} - {err}')
                await message.answer('Что - то не так с ссылкой. Проверь.', reply_markup=main_buttons)
                await state.finish()
            except KeyError as err:
                logging.info(f'-------- {message.from_user.full_name} - {err}')
                await message.answer('Что - то не так с ссылкой. Проверь.', reply_markup=main_buttons)
                await state.finish()
        else:
            logging.info(f'---- {message.from_user.full_name} - it is not url to google sheets')
            await message.answer('Это ссылка, но она ведет не на гугл табличку\n\n'
                                 'Еще разок нажми на кнопку Выкуп.', reply_markup=main_buttons)
            await state.finish()
    else:
        logging.info(f'---- {message.from_user.full_name} - it is not url')
        await message.answer('Боюсь, что это сообщение не прошло проверку на то, что это ссылка\n\n'
                             'Попробуй еще раз, у тебя все получится', reply_markup=main_buttons)
        await state.finish()

