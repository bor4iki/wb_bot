from aiogram import types
from aiogram.dispatcher import filters

from keyboards.default.main_keyboard import main_buttons
from loader import dp


@dp.message_handler(filters.Text(equals='Инструкции'))
async def bot_echo(message: types.Message):
    await message.answer(f"Играй по правилам, не присылай мне то, что я не знаю.", reply_markup=main_buttons)
