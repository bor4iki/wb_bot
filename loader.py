from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from PIL import ImageFont

from data import config

bot = Bot(token=config.BOT_TOKEN, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
FONT = ImageFont.truetype('data/RobotoFlex[slnt,wdth,wght,opsz].ttf', size=30)
